#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:60311ee4e4d975ba340a8a481f5b4b192863a7a4; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:41a88d8ab6ca38e1ee3a2bf0e1e30f63ec4f513e \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:60311ee4e4d975ba340a8a481f5b4b192863a7a4 && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
